import {
    attribute,
    hashKey,
    table,
} from '@aws/dynamodb-data-mapper-annotations';


class Transaction {
    @attribute()
    memberId: number;

    @attribute()
    icon: string;

    @attribute()
    excoCode: string;
}

@table('TransactionHistory')
class TransactionHistory {
    @hashKey()
    memberId: number;

    @attribute()
    transactions: Array<Transaction>;
}

export default TransactionHistory;