// TODO: Use TypeGraphQL to generate a schema based on a class / model like this one below
class Card {
    id: string;
    name: string;

    constructor(object: any) {
        this.id = object.sys.id;
        this.name = object.fields.title;
    }
}

export default Card;