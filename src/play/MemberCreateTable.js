
/**
 * Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License. A copy of
 * the License is located at
 *
 * http://aws.amazon.com/apache2.0/
 *
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
*/
var AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-2",
    endpoint: "http://localhost:8000"
});

var dynamodb = new AWS.DynamoDB();

var params = {
    TableName: "TransactionHistory",
    KeySchema: [
        { AttributeName: "memberId", KeyType: "HASH" },  //Partition key
        // { AttributeName: "transactions", KeyType: "RANGE" },  //Sort key
    ],
    AttributeDefinitions: [
        { AttributeName: "memberId", AttributeType: "N" },
        // { AttributeName: "transactions", AttributeType: "L" },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    }
};

dynamodb.createTable(params, function (err, data) {
    if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
        addItem();
    }
});

const putItemParams = {
    ReturnConsumedCapacity: "TOTAL",
    TableName: "TransactionHistory",
    Item: {
        "memberId": {
            "N": "4222060"
        },
        "transactions": {
            "L": [
                {
                    "M": {
                        "memberId": {
                            "S": "4222060"
                        },
                        "icon": {
                            "S": "FLIGHTS"
                        },
                        "excoCode": {
                            "S": "EK"
                        }
                    }
                },
                {
                    "M": {
                        "memberId": {
                            "S": "4222060"

                        },
                        "icon": {
                            "S": "xx"
                        },
                        "excoCode": {
                            "S": "QF"
                        }
                    }
                },
                {
                    "M": {
                        "memberId": {
                            "S": "4222060"
                        },
                        "icon": {
                            "S": "CARDSBANKING"
                        },
                        "excoCode": {
                            "S": "AMXQF07"
                        }
                    }
                }
            ]
        }
    }
};

function addItem() {
    dynamodb.putItem(putItemParams, (err, data) => {
        if (err) {
            console.error("Unable to putItem in table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Created item in table. Table result JSON:", JSON.stringify(data, null, 2));
            getItem()
        }
    });
}

function getItem() {
    console.log('start getItem')
    const params = {
        TableName: "TransactionHistory",
        Key: { "memberId": { "N": "4222060" } }
    };

    dynamodb.getItem(params , (err, data) => {
        if (err) {
            console.error("Unable to getItem in table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Got item in table. Table result JSON:", JSON.stringify(data, null, 2));
        }
    });
}