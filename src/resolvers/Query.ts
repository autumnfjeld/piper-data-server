import { getUserId, Context } from '../utils'
import { DynamoDB, Endpoint } from 'aws-sdk';
import CardService from '../services/CardService';
import Card from '../models/Card';
import TransactionHistory from '../models/TransactionHistory';
import TransactionHistoryService from '../services/TransactionHistoryService';

export const Query = {
  feed(parent, args, ctx: Context) {
    return ctx.prisma.posts({ where: { published: true } })
  },

  drafts(parent, args, ctx: Context) {
    const id = getUserId(ctx)

    const where = {
      published: false,
      author: {
        id,
      },
    }

    return ctx.prisma.posts({ where })
  },

  post(parent, { id }, ctx: Context) {
    return ctx.prisma.post({ id })
  },

  me(parent, args, ctx: Context) {
    const id = getUserId(ctx)
    return ctx.prisma.user({ id })
  },

  allCards(parent, args, ctx: Context) {
    const cardService = new CardService();

    return cardService.findAll().then((data) => {
      // console.log(data.items);
      return data.items.map(item => new Card(item));
    });
    // TODO:
  },

  transactionHistory(parent, { memberId }, ctx: Context) {
    const transactionHistoryService = new TransactionHistoryService();
    const primaryKey = 4222060;
    return transactionHistoryService.find(memberId);
  }

}

function dynamoDB() {
  const connectionOptions = {
    endpoint: new Endpoint('http://localhost:8000'),
    region: 'ap-southeast-2',
  };
  // @ts-ignore
  const client = new DynamoDB(connectionOptions)

  // TODO: remove this check when appropriate
  client.listTables((err, data) => console.log('listTables', err, data));

  // TODO: get data
}