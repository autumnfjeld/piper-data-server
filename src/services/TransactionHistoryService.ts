import { DataMapper } from '@aws/dynamodb-data-mapper';
import { DynamoDB, Endpoint } from 'aws-sdk';

import  TransactionHistory from '../models/TransactionHistory';
import { env } from '../env';


const connectionOptions = {
    endpoint: new Endpoint(env.dynamodb.endpoint),
    region: env.dynamodb.region,
};
// @ts-ignore
const client = new DynamoDB(connectionOptions);
const mapper = new DataMapper({ client });


class TransactionHistoryService {
    find(memberId: any): any {
        const transactionHistory = new TransactionHistory();
        transactionHistory.memberId = memberId;
        return mapper.get({ item: transactionHistory })
    }
}


export default TransactionHistoryService;