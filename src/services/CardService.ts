import { createClient } from 'contentful';

// TODO: Load this from .env!
const client = createClient({
    space: "cn6rg1bsbtm1",
    accessToken: "01a2a2cbae5015965cc2d3b07a1d37dfb1fa1c5a167aedb3ab596392bcf05a09"
});

// TODO: we will call Contentful for the exco mappings, which is used in business logic, not part of model
class CardService {
    find(id: string): any {
        return client.getEntry(id);
    }

    findAll() {
        return client.getEntries({
            content_type: 'card'
        });
    }

   
}

export default CardService;