import * as dotenv from 'dotenv';
import * as path from 'path';

import * as pkg from '../package.json';
import {
    getOsEnv
} from './helpers/utils';

/**
 * Load  .env.* file according to enviroment
 */


 // TODO revist once we are running in AWS environemnt  
const getDotEnvConfigPath = () => {
    const processEnv = process.env.NODE_ENV;
    let envPath;
    switch (processEnv) {
        case 'test':
            envPath = '.env.test';
            break;
        case 'production':
            envPath = '.env';
            break;
        case 'development':
            envPath = '.env.development';
            break;
        default:
            envPath = '.env.development';
    }
    console.log(`process.env.NODE_ENV = ${process.env.NODE_ENV}. Using ${envPath} for env variables`);
    return path.join(process.cwd(), envPath);
};

dotenv.config({ path: getDotEnvConfigPath() });

/**
 * Environment variables
 */
export const env = {
    isProduction: process.env.NODE_ENV === 'production',
    isTest: process.env.NODE_ENV === 'test',
    isDevelopment: process.env.NODE_ENV === 'development',
    app: {
        name: getOsEnv('APP_NAME'),
        version: (pkg as any).version,
        description: (pkg as any).description,
    },
    dynamodb: {
        endpoint: getOsEnv('AWS_DYNAMODB_ENDPOINT'),
        region: getOsEnv('AWS_REGION'),
    },
    contentful: {}
};

export default env;